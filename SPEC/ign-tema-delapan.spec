Summary:IGOS Nusantara THEMES
Name:ign-tema-delapan
Version:8.0
Release:12.8.14
License:GPLv2
Group:System Environment/Base
URL:http://igos-nusantara.or.id
Source0:%{name}.tar.gz
BuildRoot:%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:noarch
Requires: bash

%description
IGN THEMES FOR MATE DESKTOP

%prep
%setup -q -n %{name}

%install
make install PREFIX=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files

%dir 
/etc/skel/
/usr/share/themes/
/usr/share/backgrounds/
/home/igos/.mateconf/
%config 
%attr(0755,root,root) 
/usr/share/themes/*
/usr/share/backgrounds/*
%post
chown igos:igos -R /home/igos/.mateconf/
chown igos:igos -R /home/igos/.mateconf/*
%changelog
* Wed Feb 9 2012 Ibnu Yahya <ibnu.yahya@toroo.org>
- build package
